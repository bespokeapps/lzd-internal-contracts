<?php
declare(strict_types = 1);

/**
 * @author  Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 12/15/2017, 09:48
 * @package Ph\Internal\Contracts
 */

namespace Ph\Internal\Contracts;

/**
 * Interface IConsolidatedReportGenerator
 *
 * @package Ph\Internal\Contracts
 */
interface IConsolidatedReportGenerator extends ISinglePackageReporter
{
    /**
     * @param IReportRequestContext $requestContext
     * @param callable|null $log
     * @param bool $isProvisions
     * @param bool $failed
     * @return array
     */
    public function generateReport(
        IReportRequestContext $requestContext,
        callable $log = null,
        $isProvisions = true,
        $failed = false
    ): array;

    /**
     * @param IReportRequestContext $requestContext
     *
     * @return int
     * @throws \InvalidArgumentException
     */
    public function countDelivered(IReportRequestContext $requestContext): int;

    /**
     * @param IReportRequestContext $requestContext
     *
     * @return int
     * @throws \InvalidArgumentException
     */
    public function countFailed(IReportRequestContext $requestContext): int;

    /**
     * @param IReportRequestContext $requestContext
     * @param callable $log
     * @param bool $isProvisions
     * @return array
     */
    public function generateFailedReport(
        IReportRequestContext $requestContext,
        callable $log = null,
        $isProvisions = true
    ): array;
}
