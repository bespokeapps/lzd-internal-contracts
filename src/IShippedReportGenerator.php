<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 5/11/2017, 14:01
 */

namespace Ph\Internal\Contracts;

interface IShippedReportGenerator
{
    /**
     * @param IReportRequestContext $requestContext
     *
     * @param callable              $log
     *
     * @return array
     * @throws \InvalidArgumentException
     */
    public function generateShippedReport(IReportRequestContext $requestContext, callable $log = null): array;
}
