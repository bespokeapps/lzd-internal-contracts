<?php
declare(strict_types=1);

/**
 * Created by: dapo <o.omonayajo@gmail.com>
 * Created on: 17/07/2018, 1:55 PM
 */

namespace Ph\Internal\Contracts\Middleware\Loggers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Interface IHttpLogFormatter
 *
 * @package Ph\Internal\Contracts\Middleware\Loggers
 */
interface IHttpLogFormatter
{
    /**
     * @param Request $request
     * @param Response $response
     * @return array
     * @throws \LogicException
     * @throws \InvalidArgumentException
     */
    public function prepare(Request $request, Response $response): array;
}
