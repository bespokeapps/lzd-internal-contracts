<?php
declare(strict_types = 1);

/**
 * @author  Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 12/15/2017, 09:56
 * @package Ph\Internal\Contracts\Traits
 */

namespace Ph\Internal\Contracts\Traits;

use Ph\Internal\Contracts\IModuleHelper;
use Ph\Internal\Contracts\IReportRequestContext;
use Illuminate\Database\Connection;

/**
 * Trait TReportConsolidatedCounter
 *
 * @package Ph\Internal\Contracts\Traits
 */
trait TReportConsolidatedCounter
{
    /**
     * @var IModuleHelper $helper
     */
    protected $helper;

    /**
     * @var Connection $connection
     */
    protected $connection;

    /** @noinspection PhpUnusedParameterInspection */
    /**
     * @param IReportRequestContext $requestContext
     *
     * @return int
     * @throws \InvalidArgumentException
     */
    public function countDelivered(IReportRequestContext $requestContext): int
    {
        return 0;
    }

    /**
     * @param IReportRequestContext $requestContext
     *
     * @return int
     * @throws \InvalidArgumentException
     */
    public function countFailed(IReportRequestContext $requestContext): int
    {
        return 0;
    }
}
