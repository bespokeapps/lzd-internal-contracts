<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 4/5/2017, 15:40
 */

namespace Ph\Internal\Contracts\Traits;

trait TReportGeneratorLogger
{
    /**
     * @var callable $logger
     */
    protected $logger;

    /**
     * @param $message
     */
    protected function writeLog($message): void
    {
        if ($this->logger !== null && is_callable($this->logger)) {
            call_user_func($this->logger, $message);
        }
    }
}
