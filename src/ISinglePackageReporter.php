<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 5/10/2017, 17:36
 */

namespace Ph\Internal\Contracts;

interface ISinglePackageReporter extends IReportGenerator
{
    /**
     * @param \stdClass $row
     *
     * @param bool $isProvisions
     * @return array
     */
    public function singlePackagePayables(\stdClass $row, bool $isProvisions = true): array;
}
