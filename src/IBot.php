<?php
declare(strict_types = 1);

/**
 * @author  Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 3/20/2018, 17:34
 * @package Ph\Internal\Base
 */

namespace Ph\Internal\Contracts;

/**
 * Interface IBot
 *
 * @package Ph\Internal\Contracts
 */
interface IBot
{
    /**
     * @param string $message
     * @return bool
     */
    public function sendPlainMessage(string $message): bool;

    /**
     * @param string $message
     * @param array $numbers
     * @return bool
     */
    public function notifyMe(string $message, array $numbers = []): bool;

    /**
     * @param string $title
     * @param string $message
     *
     * @return bool
     */
    public function sendRichMessage(string $title, string $message): bool;
}
