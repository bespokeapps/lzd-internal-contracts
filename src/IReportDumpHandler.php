<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 4/24/2017, 14:49
 */

namespace Ph\Internal\Contracts;

/**
 * Fetch report without computations (Introduced for LEX)
 */
interface IReportDumpHandler
{
    const DELIVERED = 'DELIVERED';
    const FAILED    = 'FAILED';

    /**
     * @param IReportRequestContext $requestContext
     * @param callable              $logger
     *
     * @return array
     */
    public function dumpReport(IReportRequestContext $requestContext, callable $logger = null): array;

    /**
     * @return array
     */
    public function getSupportedTypes(): array;
}
