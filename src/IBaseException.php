<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 4/3/2017, 14:05
 */

namespace Ph\Internal\Contracts;

/**
 * Signature exception for this app
 */
interface IBaseException extends \Throwable
{

}
