<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 5/11/2017, 14:18
 */

namespace Ph\Internal\Contracts;

interface IShippedReportCounter extends IShippedReportGenerator
{
    /**
     * @param IReportRequestContext $requestContext
     *
     * @return int
     */
    public function countShippedRecords(IReportRequestContext $requestContext): int;
}
