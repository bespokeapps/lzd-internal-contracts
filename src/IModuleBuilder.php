<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 4/3/2017, 13:51
 */

namespace Ph\Internal\Contracts;

/**
 * Register Module handlers
 */
interface IModuleBuilder
{

    /**
     * @param callable $generatorCallback
     *
     * @return mixed
     */
    public function registerConsolidatedReportGenerator(callable $generatorCallback): IModuleBuilder;
}
