<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 4/3/2017, 14:21
 */

namespace Ph\Internal\Contracts;

interface IReportRequestContext
{
    /**
     * @return string
     */
    public function getProvider(): string;

    /**
     * @return string
     */
    public function getFromDate(): string;

    /**
     * @return string
     */
    public function getToDate(): string;

    /**
     * @return int
     */
    public function getRowsPerPage(): int;

    /**
     * @return int
     */
    public function getPage(): int;

    /**
     * @return bool
     */
    public function fetchAll(): bool;

    /**
     * @param int $page
     *
     * @return IReportRequestContext
     */
    public function setPage(int $page): IReportRequestContext;

    /**
     * @param int $perPage
     *
     * @return IReportRequestContext
     */
    public function setPerPage(int $perPage): IReportRequestContext;

    /**
     * @param bool $isAll
     *
     * @return IReportRequestContext
     */
    public function setIsAll(bool $isAll): IReportRequestContext;

    /**
     * @param string $fromDate
     *
     * @return IReportRequestContext
     */
    public function setFromDate(string $fromDate): IReportRequestContext;

    /**
     * @param string $toDate
     *
     * @return IReportRequestContext
     */
    public function setToDate(string $toDate): IReportRequestContext;

    /**
     * @param string $provider
     *
     * @return IReportRequestContext
     */
    public function setProvider(string $provider): IReportRequestContext;
}
