<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 6/21/2017, 18:19
 */

namespace Ph\Internal\Contracts;

interface IScorecardComputer
{
    /**
     * @param string $provider
     * @param int    $period
     *
     * @return array
     */
    public function getScoreCard(string $provider, int $period = 3): array;

    /**
     * @param int $period
     * @param bool $byProvince
     * @return array
     */
    public function generateAllCards(int $period, bool $byProvince = false): array;
}
