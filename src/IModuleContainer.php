<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 4/3/2017, 13:54
 */

namespace Ph\Internal\Contracts;

interface IModuleContainer
{
    /**
     * Get a module service
     *
     * @param string $serviceName
     *
     * @return mixed
     */
    public function get(string $serviceName);

    /**
     * @return IConsolidatedReportGenerator
     * @throws IBaseException
     */
    public function getConsolidatedReporter(): IConsolidatedReportGenerator;
}
