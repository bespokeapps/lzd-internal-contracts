<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 5/12/2017, 10:56
 */

namespace Ph\Internal\Contracts;

interface IFailedDeliveryReportGenerator extends IReportGenerator
{

    /**
     * @param IReportRequestContext $requestContext
     *
     * @return int
     */
    public function countRecords(IReportRequestContext $requestContext): int;
}
