<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 4/3/2017, 13:49
 */

namespace Ph\Internal\Contracts;

interface IModuleProvider
{
    /**
     * @param IModuleBuilder $builder
     */
    public function registerModuleHandlers(IModuleBuilder $builder): void;

    /**
     * Fetch module's provider identifier
     *
     * @return string
     */
    public function getModuleName(): string;
}
