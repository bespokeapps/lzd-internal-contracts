<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 4/4/2017, 15:28
 */

namespace Ph\Internal\Contracts;

interface IModuleHelper
{

    /**
     *
     * @param string $from
     * @param string $toDate
     * @param string $week
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    public function getFilterCondition(string $from = null, string $toDate = null, string $week = null): string;

    /**
     * @param string|null $from
     * @param string|null $toDate
     * @param string|null $week
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    public function getFailedDeliveryFilterCondition(
        string $from = null,
        string $toDate = null,
        string $week = null
    ): string;

    /**
     * @param IReportRequestContext $requestContext
     *
     * @return array
     */
    public function extractParams(IReportRequestContext $requestContext): array;

    /**
     * @param \stdClass $row
     * @param bool $ceiled
     * @param int $maxWeight
     * @param int $defaultWeight
     *
     * @return mixed
     */
    public function getChargeableWeight(\stdClass $row, $ceiled = false, $maxWeight = -1, $defaultWeight = 1);

    /**
     * @param \stdClass $row
     * @param bool $ceiled
     * @param int $maxWeight
     *
     * @return mixed
     */
    public function getReverseBillingChargeableWeight(\stdClass $row, $ceiled = false, $maxWeight = -1);

    /** @noinspection MoreThanThreeArgumentsInspection */
    /**
     * @param string $provider
     * @param int $weightDivisor
     * @param string $customFields
     * @param bool $isProvisions
     * @param bool $failed
     * @return string
     */
    public function getConsolidatedQuery(
        string $provider,
        int $weightDivisor = 1,
        string $customFields = '',
        bool $isProvisions = true,
        bool $failed = false
    ): string;

    /** @noinspection MoreThanThreeArgumentsInspection */
    /**
     * @param \stdClass $row
     * @param           $key
     * @param array     $payables
     * @param string    $notToProcess
     * @param array     $results
     */
    public function prepareRow(
        \stdClass $row,
        $key,
        array $payables,
        string $notToProcess,
        array &$results
    ): void;

    /**
     * @return int | float
     */
    public function getVat();

    /**
     * @param string $paymentMethod
     *
     * @return bool
     */
    public function isCOD($paymentMethod): bool;

    /**
     * @param \stdClass $row
     *
     * @return bool
     */
    public function packageDelivered(\stdClass $row): bool;
}
