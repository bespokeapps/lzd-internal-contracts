<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 4/3/2017, 13:48
 */

namespace Ph\Internal\Contracts;

interface IModuleManager
{
    /**
     * @param string $module
     *
     * @return bool
     */
    public function has(string $module): bool;

    /**
     * @param IModuleProvider $provider
     * @throws IBaseException
     */
    public function add(IModuleProvider $provider): void;

    /**
     * @param string $module
     *
     * @return IModuleContainer
     * @throws IBaseException
     */
    public function get(string $module): IModuleContainer;

}
