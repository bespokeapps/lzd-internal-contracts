<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 4/3/2017, 11:35
 */

namespace Ph\Internal\Contracts;

interface IReportGenerator
{
    const BULKY                 = 'SINGLE_ITEM_OR_BULKY';
    const POUCH                 = 'POUCH';
    const BUNDLED               = 'BUNDLED';
    const ODZ                   = 'ODZ';
    const NOT_PROCESSED         = 'NOT_TO_PROCESS';
    const OVER_CHARGED          = 'EXCESSIVE_DIM_WEIGHT';
    const BOB_DATA              = 'BOB_DATA';

    const POUCH_SIZES           = [ 'SMALL', 'S', 'MEDIUM', 'M', 'LARGE', 'L', 'X-LARGE', 'XL' ];
    const SMALL_POUCHES         = [ 'SMALL', 'S' ];
    const MEDIUM_POUCHES        = [ 'MEDIUM', 'M' ];
    const LARGE_POUCHES         = [ 'LARGE', 'L', 'X-LARGE', 'XL' ];

    /**
     * @return array
     */
    public function getSupportedPackageTypes(): array;
}
