<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 4/3/2017, 14:52
 */

namespace Ph\Internal\Contracts;

interface ILogger
{
    public function info(string $message);
}
