<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 3/28/2017, 16:57
 */

namespace Ph\Internal\Contracts;

interface ILoginHandler
{
    /**
     * Attempt login
     *
     * @return bool
     */
    public function handle(): bool;

    /**
     * Get Auth error if any
     *
     * @return string
     */
    public function getError(): string;

    /**
     * @return mixed
     */
    public function getToken();
}
