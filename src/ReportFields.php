<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 4/20/2017, 11:26
 */

namespace Ph\Internal\Contracts;

define('FLD_VALUATION', 'Valuation');
define('FLD_COLLECTION_FEE', 'CollectionFee');
define('FLD_TARGET_SHEET', 'TargetSheet');
define('FLD_REMARKS', 'REMARKS');
define('FLD_VAT', 'VAT');
define('FLD_TOTAL', 'Total');
define('FLD_ZONE', 'Zone');
define('FLD_PACKAGE_SIZE', 'PackageSize');
define('FLD_FREIGHT_CHARGE', 'FreightCharge');
define('FLD_PACKAGE_ITEMS', 'PackageItems');
define('FLD_RTS_FEE', 'RTSFee');
define('FLD_PICKUP_FEE', 'PickupFee');
define('FLD_CHANNEL', 'Channel');
define('FLD_ORDER_TYPE', 'Order Type');

final class ReportFields
{
    const FLD_VALUATION         = FLD_VALUATION;
    const FLD_COLLECTION_FEE    = FLD_COLLECTION_FEE;
    const FLD_TARGET_SHEET      = FLD_TARGET_SHEET;
    const FLD_REMARKS           = FLD_REMARKS;
    const FLD_VAT               = FLD_VAT;
    const FLD_TOTAL             = FLD_TOTAL;
    const FLD_ZONE              = FLD_ZONE;
    const FLD_PACKAGE_SIZE      = FLD_PACKAGE_SIZE;
    const FLD_FREIGHT_CHARGE    = FLD_FREIGHT_CHARGE;
    const FLD_PACKAGE_ITEMS     = FLD_PACKAGE_ITEMS;
    const FLD_RTS_FEE           = FLD_RTS_FEE;
    const FLD_PICKUP_FEE        = FLD_PICKUP_FEE;
    const FLD_CHANNEL           = FLD_CHANNEL;
    const FLD_ORDER_TYPE        = FLD_ORDER_TYPE;

    const LINE_ID           = 'Line_ID';
    const TPL_NAME          = '3PL_Name';
    const DELIVERY_STATUS   = 'Delivery_Status'; // Delivered / Failed
    const PICKUP_DATE       = 'Package_PickUp_Date'; // Empty
    const SHIP_DATE         = 'Package_Ship_Date';
    const DELIVERY_DATE     = 'Package_POD_Date';
    const FAILED_DATE       = 'Package_Failed_Date';
    const INVOICE_NUMBER    = 'Invoice_Number'; // Empty
    const PACKAGE_NUMBER    = 'Lazada_Package_Number';
    const TRACKING_NUMBER   = '3PL_Tracking_Number';
    const R_TRACKING_NUMBER = 'Tracking_Number_Return'; // Empty
    const ORDER_NUMBER      = 'Order_Number';
    const SERVICE_TYPE      = 'Shipping Service Type'; // Default: Standard
    const DECLARED_VALUE    = 'Goods Value';
    const TO_BE_COLLECTED   = 'Amount to be Collected';
    const PACKAGE_VOLUME    = 'Package Volume';
    const DIM_WEIGHT        = 'Package Volumetric';
    const PACKAGE_HEIGHT    = 'Package_Height';
    const PACKAGE_WIDTH     = 'Package_Width';
    const PACKAGE_LENGTH    = 'Package_Length';
    const PACKAGE_WEIGHT    = 'Package_Weight';
    const CHARGEABLE_WEIGHT = 'Package_Chargeable_Weight';
    const SHIPPING_RATE     = 'Shipping Rate'; // Empty
    const PICKUP_FEE        = 'Pickup_Fee'; // Empty
    const DELIVERY_FEE      = 'Delivery Fee'; // Empty
    const FREIGHT_CHARGE    = 'Carrying_Fee';
    const REDELIVERY_FEE    = 'Redelivery_Fee'; // Empty
    const REJECTION_FEE     = 'Rejection_Fee'; // Empty
    const COD_FEE           = 'COD_Fee';
    const RTS_FEE           = 'Failed Delivery Fee';
    const ODZ_SURCHARGES    = 'Special_Area_Fee';
    const HANDLING_FEE      = 'Special_Handling_Fee'; // Empty
    const VALUATION_FEE     = 'Insurance_Fee';
    const PORTERAGE_FEE     = 'Porterage Fee'; // Empty (2go Sea Freight only)
    const FUEL_SURCHARGE    = 'Fuel_Surcharge_Fee'; // Empty
    const STAMP_FEE         = 'DOC_Stamp_Fee'; // Empty
    const ORIGIN_BRANCH     = 'Origin_Branch'; // Empty
    const DESTINATION       = 'Destination_Branch'; // Empty
    const ZIP_CODE          = 'Delivery_Zone_Zip_Code'; // Empty
    const PACKAGE_TYPE      = 'Rate_Type';
    const PACKAGE_COUNT     = 'Number of Items';
    const VAT               = 'VAT';
    const TOTAL             = 'Total_Payment';
    const PAYMENT_METHOD    = 'Payment Method';
    const CITY              = 'Shipping City';
    const REGION            = 'Region';
    const AREA              = 'Province';
    const BARANGAY          = 'Barangay';
    const FULFILMENT_TYPE   = 'Fulfilment Type'; // Empty
    const SKU               = 'SKU';
    const SKU_DESCRIPTION   = 'SKU Description';
    const DIM_WEIGHT_REF    = 'DIM WEIGHT REF';
    const WARE_HOUSE        = 'ORIGIN WAREHOUSE';
}
