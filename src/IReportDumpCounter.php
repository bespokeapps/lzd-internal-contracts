<?php
declare(strict_types = 1);

/**
 * @author Oladapo Omonayajo <oladapo.omonayajo@lazada.com.ph>
 * Created on 4/25/2017, 11:33
 */

namespace Ph\Internal\Contracts;

interface IReportDumpCounter
{
    /**
     * @param IReportRequestContext $requestContext
     * @param callable              $logger
     *
     * @return int
     */
    public function countRecords(IReportRequestContext $requestContext, callable $logger = null): int;
}
